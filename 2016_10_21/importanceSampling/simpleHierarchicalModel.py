import scipy.stats as ss
import numpy as np
import numpy.random as npr
from math import sqrt, log


class hierarchy:
    def __init__(self, G, D, A, B):
        self.G = G
        self.D = D
        self.A = A
        self.B = B

    def unnorm_log_pdf(self, mu, lam):
        return  ss.norm.logpdf(  x=mu,  loc=self.G, scale=1./sqrt(self.D) ) +\
                ss.gamma.logpdf( x=lam, a=self.A , scale=1./self.B )


class hierarchy_conditional(hierarchy):
    '''A simplified hierarchical model.'''
    def __init__(self, observedData, G, D, A, B):
        super(hierarchy_conditional,self).__init__(G, D, A, B)
        self.X      = observedData
        self.n      = len(observedData)
        self.mean   = observedData.mean()
        self.var    = observedData.var()
        self.params = ['mu', 'lam']
        self.rv     = {'mu': self.rv_mu, 'lam':self.rv_lam }
        # self.rv = dict( (x, getattr(self, 'rv_'+x) ) for x in self.params ) # This approach uses my naming convention for random variable generators

    def rv_lam(self, lam, mu):
        denominator = self.n*( self.var+(mu-self.mean)**2 )/2. + self.B
        return ss.gamma.rvs(
            a     = self.n/2. + self.A,
            scale = 1./denominator   )

    def rv_mu(self, lam, mu):
        denominator = lam*self.n+self.D
        return ss.norm.rvs(
            loc     = ( lam*self.n*self.mean+self.G*self.D )/denominator,
            scale   = 1./denominator )

    def unnorm_log_pdf(self, lam, mu):
        res = super(hierarchy_conditional, self).unnorm_log_pdf(lam, mu)
        res += self.n/2. * ( log(lam) - lam*(self.mean - mu)**2 )
        return res


class hierarchy_full(hierarchy):
    def __init__(self, G, D, A, B,n ):
        super(hierarchy_full, self).__init__(G, D, A, B)
        self.n = n

    def rvState(self):
        P = {}
        P['mu']  = ss.norm.rvs( loc= self.G, scale = 1./sqrt(self.D)  )
        P['lam'] = ss.gamma.rvs( a = self.A, scale = 1./self.B )
        return P

    def rvState_and_observations(self):
        P = self.rvState()
        X = ss.norm.rvs( loc=P['mu'], scale=1/sqrt( P['lam'] ), size=self.n )
        return P, X
