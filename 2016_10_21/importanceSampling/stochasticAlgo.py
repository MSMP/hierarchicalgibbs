class MC:
	def __init__(self, state0, stepsNo, burnIn):
		self.stepsNo = stepsNo
        self.burnIn  = burnIn
        self.state   = state0.copy()
    
    def run(self):
    	states = []
    	for i in range(self.burnIn + self.stepsNo):
    		if i == self.burnIn - 1:
                collectData = True
            self.update()        
            if collectData: 
                states.append(self.state.copy())
        return states

    def update():
    	raise NotImplementedError
