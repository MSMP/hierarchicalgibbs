from stochasticAlgo import MC

class importanceSampling(MC):
	"""The importance sampling algorithm."""
	def __init__(self, nominal, importance, state0, stepsNo, burnIn ):
		super(importanceSampling, self).__init__(state0, stepsNo, burnIn)
        self.nominal 	= nominal
        self.importance = importance
            
    def update(self):
    	self.state = self.importance.sample()