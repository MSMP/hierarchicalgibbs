import scipy.stats as ss 
import numpy as np
import numpy.random as npr 
from math import sqrt

def generateSample(G, D, A, B, n):
    P = {}      # model parameters are mu and lam.
    # X =       # The sample we will observe.    
    # return X, P
    pass

def rLam(var, mean, n, lam, mu, G, D, A, B):
    pass 
    
def rMu(var, mean, n, lam, mu, G, D, A, B):
    pass

marginals = { 'lam':rLam, 'mu':rMu }

def systematicUpdate(Y, params, hyper):
    # for m in marginals:
        # do something with params
    return params
    

def randomUpdate(Y, params, hyper):
    # select a random parameter name with npr.choice
    # update the params
    return params

def Gibbs(Y, params, hyper, update, simLen = 5000):
    states = [ params.copy() ]    
    # write a loop where you run the update chosen from the two above
    return states
