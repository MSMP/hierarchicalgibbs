import scipy.stats as ss 
import numpy as np
import numpy.random as npr 
from math import sqrt, log


class hierarchy:
    def __init__(self, G, D, A, B):
        # set up the hyper-parameters as class attributes    

    def unnorm_log_pdf(self, mu, lam):
        # implement the density of pair (mu, lam) given the hyperparameters (the middle level of the hierarchy in the model)

class hierarchy_conditional(hierarchy):
    '''A simplified hierarchical model.'''
    def __init__(self, observedData, G, D, A, B):
        super(hierarchy_conditional,self).__init__(G, D, A, B)
        self.X      = observedData
        self.n      
        self.mean   
        self.var    
        self.params = ['mu', 'lam']
        self.rv     = {'mu': self.rv_mu, 'lam':self.rv_lam }
        # self.rv = dict( (x, getattr(self, 'rv_'+x) ) for x in self.params ) # This approach uses my naming convention for random variable generators  

    def rv_lam(self, lam, mu):
        
    def rv_mu(self, lam, mu):
        
    def unnorm_log_pdf(self, lam, mu):
        # the unnormalised likelihood of 
        #   (data, state) | hyperparameters
        res = super(hierarchy_conditional, self).unnorm_log_pdf(lam, mu) 
        # res += 
        return res      

class hierarchy_full(hierarchy):
    def __init__(self, G, D, A, B,n ):
        super(hierarchy_full, self).__init__(G, D, A, B)
        self.n = n

    def rvState(self):
        '''Returns a random state given the hyperparameters.'''
        P = {}        
        return P

    def rvState_and_observations(self):
        '''Returns both a random state and data given the hyperparameters.'''
        # return P, X
        pass


    