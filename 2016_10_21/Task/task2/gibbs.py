import numpy.random as npr 

class Gibbs:
    '''The Gibbs algorithm: a quasi virtual class.'''
    def __init__(self, model, state0, stepsNo, burnIn ):
        # self.stepsNo =
        # self.model   =
        # self.state   =
        # self.burnIn  =

    def update(self):
        raise NotImplementedError
    
    def run(self):
        # Encode a for loop that will run the simulation
        # first in 'burn in' mode (simulation results are not stored),
        # and then switches to the normal mode.

        # Use the self.update function. 

        # Remember about the mutability of the dictionary.
        states = []
        collectData = False # The burn in guardian.
        # for i in range(something):
        # meaningful code here!     
        return states

class GibbsRandomUpdate(Gibbs):
    '''The Gibbs algorithm with random update.'''
    def update(self):
        # As in task 1, but instead of returning anything let the model's
        # state attribute (self.state) change.
        pass

class GibbsSystematicUpdate(Gibbs):
    '''The Gibbs algorithm with systematic update.'''
    def update(self):
        # As in task 1, but instead of returning anything let the model's
        # state attribute (self.state) change.
        pass

def gibbs_algorithm(model, state0, update, stepsNo, burnIn=5000):
    '''A wrapper for calling the Gibbs algorithm.'''
    algo = {   
        'systematic':   GibbsSystematicUpdate,
        'random':       GibbsRandomUpdate 
    }[update](model, state0, stepsNo, burnIn)
    res = algo.run()
    return res
