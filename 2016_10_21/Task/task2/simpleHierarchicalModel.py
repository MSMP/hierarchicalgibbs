import scipy.stats as ss 
import numpy as np
import numpy.random as npr 
from math import sqrt

def sample(G, D, A, B, n):
    # return X, P
    pass

class simpleHierarchicalModel:
    '''A simplified hierarchical model.'''
    def __init__(self, observedData, G, D, A, B):
        self.G = G
        self.D = D
        self.A = A
        self.B = B
        self.X      = observedData
        # self.n    =  
        # self.mean = 
        # self.var  =
        self.params = ['mu', 'lam']
        self.rv     = {'mu': self.rv_mu, 'lam':self.rv_lam }
        
    def rv_lam(self, lam, mu):
        pass
    
    def rv_mu(self, lam, mu):
        pass