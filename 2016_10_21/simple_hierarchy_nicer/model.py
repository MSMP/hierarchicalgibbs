import scipy.stats as ss
import numpy as np
import numpy.random as npr
from math import sqrt

def generateSample(G, D, A, B, n):
    P = {}      # model parameters
    P['mu']     = ss.norm.rvs( loc=G, scale=1/sqrt(D)  )
    P['lam']    = ss.gamma.rvs( a=A, scale=1./B )
    X           = ss.norm.rvs( loc=P['mu'], scale=1/sqrt( P['lam'] ), size=n )
    return X, P

def rLam(var, mean, n, lam, mu, G, D, A, B):
    denominator = n*(var + (mu-mean)**2)/2. + B
    return ss.gamma.rvs( a = n/2. + A, scale = 1./denominator )

def rMu(var, mean, n, lam, mu, G, D, A, B):
    denominator = lam*n+D
    return ss.norm.rvs(
        loc     = (lam*n*mean+G*D)/denominator,
        scale   = 1./sqrt(denominator) )

marginals = { 'lam':rLam, 'mu':rMu }

def systematicUpdate(Y, params, hyper):
    for m in marginals:
        params[m] = marginals[m](**Y, **params, **hyper)
    return params

def randomUpdate(Y, params, hyper):
    rKey = npr.choice( list( marginals.keys() ) )
    params[rKey] = marginals[ rKey ](**Y, **params, **hyper)
    return params

def Gibbs(X, state, hyper, update, simLen = 5000):
    states = [ state.copy() ]
    for _ in range(simLen):
        state = update( X, state, hyper )
        states.append( state.copy() )
    return states
