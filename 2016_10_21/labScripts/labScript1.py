x = [1,5,6]

y = []

for e in x:
	y.append(3*e)

def nameOfFunction(arg0, arg1):
	res = arg1+arg0
	return res

# cond = True
# if cond2 or cond:
# 	print('Hi')
# else:
# 	print('Bye')

def F(L, scalar):
	R = []
	for l in L:
		R.append(l*scalar)
	return R

print(F(x, 1.4))
print x

def G(arg):
	return arg, arg**2

x1, x2 = G(2)


print(x1)
print(x2)