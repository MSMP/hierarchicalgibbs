class Animals:
	"""This is the most frequent way to show what are classes."""
	def __init__(self, years):		
		self.years = years
	def sound(self):
		print('I am an animal.')
	def tell_age(self):
		print('I am ' + str(self.years) + ' years old.' )

animal = Animals(4)

animal.tell_age()
animal.sound()


class Dogs(Animals):
	"""docstring for Dogs"""
	def sound(self):
		super().sound()
		print('But above all, I am a Dog.')

class Cats(Animals):
	"""docstring for Cats"""
	def sound(self):	
		super().sound()
		print('But above all, I am a Cat.')

## In python2 you would write rather
# class Dogs(Animals):
# 	"""docstring for Dogs"""
# 	def sound(self):
# 		super(Dogs, self).sound()
# 		print('But above all, I am a Dog.')

# class Cats(Animals):
# 	"""docstring for Cats"""
# 	def sound(self):	
# 		super(Cats, self).sound()
# 		print('But above all, I am a Cat.')


dog = Dogs(15)
dog.sound()
dog.tell_age()

cat = Cats(17)
cat.sound()
cat.tell_age()
