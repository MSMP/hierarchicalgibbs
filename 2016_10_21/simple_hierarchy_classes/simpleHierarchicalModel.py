import scipy.stats as ss 
import numpy as np
import numpy.random as npr 
from math import sqrt

def sample(G, D, A, B, n):
    P = {}      # model parameters
    P['mu']     = ss.norm.rvs( loc=G, scale=1./sqrt(D)  )
    P['lam']    = ss.gamma.rvs( a=A, scale=1./B )
    X           = ss.norm.rvs( loc=P['mu'], scale=1/sqrt( P['lam'] ), size=n ) 
    return X, P

class simpleHierarchicalModel:
    '''A simplified hierarchical model.'''
    def __init__(self, observedData, G, D, A, B):
        self.G = G
        self.D = D
        self.A = A
        self.B = B
        self.X = observedData
        self.n = len(observedData)
        self.mean   = observedData.mean()
        self.var    = observedData.var()
        self.params = ['mu', 'lam']
        self.rv     = {'mu': self.rv_mu, 'lam':self.rv_lam }
        # self.rv = dict( (x, getattr(self, 'rv_'+x) ) for x in self.params ) # This approach uses my naming convention for random variable generators  

    def rv_lam(self, lam, mu):
        denominator = self.n*( self.var+(mu-self.mean)**2 )/2. + self.B 
        return ss.gamma.rvs( 
            a     = self.n/2. + self.A, 
            scale = 1./denominator   )   
    
    def rv_mu(self, lam, mu):
        denominator = lam*self.n+self.D
        return ss.norm.rvs( 
            loc     = ( lam*self.n*self.mean+self.G*self.D )/denominator, 
            scale   = 1./denominator )

