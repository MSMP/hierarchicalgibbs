import numpy.random as npr 

class Gibbs:
    '''The Gibbs algorithm: a quasi virtual class.'''
    def __init__(self, model, state0, stepsNo, burnIn ):
        self.stepsNo = stepsNo
        self.model   = model
        self.state   = state0.copy()
        self.burnIn  = burnIn
    
    def update(self):
        raise NotImplementedError
    
    def run(self):
        collectData = False
        for i in range(self.burnIn + self.stepsNo):
            if i == self.burnIn:
                states = [ self.state.copy() ]
                collectData = True
            self.update()        
            if collectData: 
                states.append( self.state.copy() )
        return states

class GibbsRandomUpdate(Gibbs):
    '''The Gibbs algorithm with random update.'''
    def update(self):
        rParam = npr.choice( self.model.params )        
        self.state[rParam] = self.model.rv[rParam](**self.state)        

class GibbsSystematicUpdate(Gibbs):
    '''The Gibbs algorithm with systematic update.'''
    def update(self):
        for p in self.model.params:
            self.state[p] = self.model.rv[p](**self.state)  

def gibbs_algorithm(model, state0, update, stepsNo, burnIn=5000):
    '''A wrapper for calling the Gibbs algorithm.'''
    algo = {   
        'systematic':   GibbsSystematicUpdate,
        'random':       GibbsRandomUpdate 
    }[update](model, state0, stepsNo, burnIn)
    res = algo.run()
    return res
