import sympy as s
s.init_printing()
x, w, m1, m2 = s.symbols('x, w, m1, m2')
f = w * s.exp( - (x-m1)**2/2 ) + (1-w) * s.exp( - (x-m2)**2/2 )

s.diff(f, w)
s.diff(f, m1)
diff = s.diff(f, m2)

f_np = s.utilities.lambdify((x,w,m1,m2), f)

f_np
f_np(0,1,0,2)

f_np
