from simpleHierarchicalModel import hierarchy_conditional, hierarchy_full
import pandas as pd
from pprint import pprint as print
from math import exp, fsum, log, inf
import numpy as np

hyper = { 'G': 2.,'D': 4.,'A': 2.,'B': 1. }

importance 	= hierarchy_full( **hyper, n=200 )
realP, obsX = importance.rvState_and_observations()

# We could use a different hyper.
# In real life we would definitely do that.
nominal = hierarchy_conditional(obsX, **hyper)

stepsNo = 10000
states 	= []
logWeights = []

for _ in range(stepsNo):
	state = importance.rvState()
	states.append(state)
	logQ = importance.unnorm_log_pdf(**state)
	logP = nominal.unnorm_log_pdf(**state)
	logWeights.append(logP-logQ)

# fsum claims to return a more accurate floating point number than sum.

logSumOfWeights = log(fsum( exp(lW) for lW in logWeights))
logWeights = [ lW-logSumOfWeights for lW in logWeights ]

def save(thing, path):
	pd.DataFrame(thing).to_csv(path, index=False)

save(states, 'importanceSampling.csv')
save(logWeights, 'logWeights.csv')
save([realP], 'realP.csv')
